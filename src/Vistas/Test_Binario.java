/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import Modelo.Binario;

/**
 *
 * @author madarme
 */
public class Test_Binario {

    public static void main(String[] args) {
        /*
            el constructor SÓLO RECIBE NUMEROS POSITIVOS
        */
        Binario uno = new Binario(290);
        Binario dos = new Binario(12);
        Binario tres = uno.getSuma(dos);
        Binario cuatro = uno.getResta(dos);
        System.out.println("La suma de:" + uno.toString() + "+" + dos.toString() + " es igual a:" + tres.toString());
        System.out.println("La resta de:" + uno.toString() + "+" + dos.toString() + " es igual a:" + tres.toString());

    }

}
