/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import Modelo.Monomio;
import Modelo.Persona;

/**
 * Clase de prueba para Objetos Monomios
 *
 * @author madarme
 */
public class TestMonomio {

    public static void main(String[] args) {

        Monomio m1 = new Monomio(23, 'x', -2);
        Monomio m2 = new Monomio(23, 'x', -2);
        System.out.println(m1.toString());
        System.out.println(m2.toString());

        //Por alguna extraña razon toca castear el short para que no tire error
        
        Persona nadie1 = new Persona(1214742378, (short)1998, (short)3, (short)30, (short)10, (short)40, (short)20, "Rafael Alfonso Sarmiento");
        Persona nadie2 = new Persona(1005024288, (short) 2000, (short) 1, (short) 6, (short) 1, (short) 50, (short) 20, "Carlos saul garcia cobos");
        System.out.println(nadie1.toString());
        System.out.println(nadie2.toString());
        
        
        boolean sonIguales1 = m1.equals(m2);
        if (sonIguales1) {
            System.out.println(m1.toString() + " ES IGUAL A :" + m2.toString());
        } else {
            System.out.println(m1.toString() + " NO SON IGUALES:" + m2.toString());
        }

        try {
            //int c = m1.compareTo(nadie);
            int c2 = m1.compareTo(m2);
            if (c2 == 0) {
                System.out.println(m1.toString() + " ES IGUAL A :" + m2.toString());
            } else if (c2 > 0) {
                System.out.println(m1.toString() + " ES MAYOR:" + m2.toString());
            } else {
                System.out.println(m1.toString() + " ES MENOR:" + m2.toString());
            }
            System.out.println("comparador:" + c2);
        } catch (Exception e) {
            System.err.println("Error :( -->" + e.getMessage());
        }
        
        
        /**
         * Se comparan ambos objetos para saber si son iguales o no
         */
        boolean sonIguales2 = nadie1.equals(nadie2);
        if (sonIguales2) {
            System.out.println(nadie1.getNombresCompletos()+ " tiene la misma edad que " + nadie2.getNombresCompletos());
        } else {
            System.out.println(nadie1.getNombresCompletos()+ " no tiene la misma edad que " + nadie2.getNombresCompletos());
        }

        try {
            /**
             * Se restan ambos objetos para saber quien es mayor o menor, o si son iguales
             */
            int c2 = nadie1.compareTo(nadie2);
            if (c2 == 0) {
                System.out.println(nadie1.getNombresCompletos() + " tiene la misma edad que " + nadie2.getNombresCompletos());
            } else if (c2 > 0) {
                System.out.println(nadie1.getNombresCompletos() + " es mayor que " + nadie2.getNombresCompletos());
            } else {
                System.out.println(nadie1.getNombresCompletos() + " es menor que " + nadie2.getNombresCompletos());
            }
            System.out.println("comparador:" + c2);
        } catch (Exception e) {
            System.err.println("Error :( -->" + e.getMessage());
        }

    }

}
