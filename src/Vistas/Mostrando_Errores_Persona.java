/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import Modelo.Persona;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rafael Sarmiento
 */
public class Mostrando_Errores_Persona {
    
    public static void main(String[] args){
        
        Persona x = new Persona(leerInt("Ingrese cedula: "), LeerAgno("Ingrese el año: "), LeerMes("Ingrese el mes: "),LeerDia("Ingrese el dia: "), LeerHora("Ingrese la hora: "), LeerMinuto("Ingrese el minuto: "), LeerMinuto("Ingrese el segundo: "),leerString("Ingrese el nombre: ")  );
        System.out.println(x.toString());
    
    }
    
    
    
    private static short LeerAgno(String msg){
        
        short a;
        try{
            a = LeerShort(msg);
            validarAgno(a);
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return LeerAgno(msg);
        }
        return a; 
    }
    
    private static void validarAgno(short a) throws Exception{
        boolean esValido = (a <= 2021  && a >= 1000);
        if(!esValido){
            throw new Exception("No es un año valido");
        }
    
    }
    
    private static short LeerShort(String msg){
        short a ;
        System.out.println(msg);
        Scanner in = new Scanner(System.in);
        a =  (in.nextShort());
        return a;
    }
    private static short LeerMes(String msg){
        
     short a;
        
        try{
            a= LeerShort(msg);
            /**
            System.out.println(msg);
            Scanner in = new Scanner(System.in);
            a =  (in.nextShort());
            * */           
            validarMes(a);
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return LeerMes(msg);
        }
        return a;
    }
    
    private static void validarMes(short a) throws Exception{
        boolean esValido = (a <= 12  && a >= 1);
        if(!esValido){
            throw new Exception("No es un mes valido");
        }
    
    }
    
    
    
    private static short LeerDia(String msg){
        
        short a;
        
        try{
            a = LeerShort(msg);
            validarDia(a);
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return LeerDia(msg);
        }
        return a;        
    }
    
    
    private static void validarDia(short a) throws Exception{
        
        //Persona.getMes();
        //boolean esValido1 = (a <= 30  && a >= 1);
        boolean esValido2 = (a <= 31 && a >= 1);
       //boolean esValido3 = (a <= 28 && a >= 1);
        if(!esValido2){
            throw new Exception("No es un dia valido");
        }
    
    }
    
    
    private static short LeerHora(String msg){
        
        short a;
        
        try{
            a = LeerShort(msg);
            validarHora(a);
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return LeerHora(msg);
        }
        return a;        
    }
    
    private static void validarHora(short a) throws Exception{
        boolean esValido = (a <= 23  && a >= 0);
        if(!esValido){
            throw new Exception("No es un mes valido");
        
    
        }
    }
    
    private static void validarMin(short a)throws Exception{
        boolean esValido = (a <= 59  && a >= 0);
        if(!esValido){
            throw new Exception("No es un mes valido");
        
    
        }
        
    
    }
    private static short LeerMinuto(String msg){
        
        short a;
        
        try{
            a = LeerShort(msg);
            validarMin(a);
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return LeerMinuto(msg);
        }
        return a;        
    }
    /**
    private static short LeerSegundo(String msg){
        
        System.out.println(msg);
            Scanner in = new Scanner(System.in);
            return (in.nextShort());        
    }*/
    
    
     private static int leerInt(String msg) {

        //Ejemplifico la captura de manera general:
        try {
            System.out.println(msg);
            Scanner in = new Scanner(System.in);
            return (in.nextInt());
        } catch (Exception e) {
            System.err.println("No es un entero:" + e.getMessage());
            return leerInt(msg);
            //return -1;
        }
    
    }
     
     private static String leerString(String msg){
         
         System.out.println(msg);
         Scanner in = new Scanner(System.in);
         return in.nextLine();
         //return leerString(msg);
         
     }
}