/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import Modelo.Monomio;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madar
 */
public class Test_Mostrando_Errores_Java {

    public static void main(String[] args) {
        //public Monomio(float coeficiente, char literal, int exponente)
        Monomio x = new Monomio(leerFloat("Digite coeficiente:"), leerChar("Digite literal:"), leerInt("Digite exp:"));
        System.out.println("Su monomio es:" + x.toString());

    }

    private static int leerInt(String msg) {

        //Ejemplifico la captura de manera general:
        try {
            System.out.println(msg);
            Scanner in = new Scanner(System.in);
            return (in.nextInt());
        } catch (Exception e) {
            System.err.println("No es un entero:" + e.getMessage());
            return leerInt(msg);
            //return -1;
        }
    }

    private static float leerFloat(String msg) {
        //opcional
        try {
            System.out.println(msg);
            Scanner in = new Scanner(System.in);
            return (in.nextFloat());
        } catch (java.util.InputMismatchException e) {
            //Esté error es especìfico
            System.out.println("Se ha producido un error , no es un Float:" + e.getMessage());
            return leerFloat(msg);
        }
    }

    private static char leerChar(String msj) {
        System.out.println(msj);
        Scanner in = new Scanner(System.in);
        char letra = in.nextLine().charAt(0);
        try {
            validarChar(letra);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return leerChar(msj);
        }

        return (letra);
    }

    /**
     * Vamos a lanzar una excepción OBLIGATORIA
     *
     * @param caracter un caracter a validar entre a y z or A y Z
     *
     */
    private static void validarChar(char caracter) throws Exception {
        // Se convierte el caracter ingresado en una cadena
        String cadena = String.valueOf(caracter);
        //Se recorre la cadena añadiendo un contador para saber cuantos caracteres hay dentro
        for (int x = 0; x < cadena.length(); x++) {
            //Se convierte el contador final y se almacena en una variable de tipo char
            char c = cadena.charAt(x);
            //Se hacen las validaciones usando la variable de tipo char
            if (!((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == ' ')) {
                throw new Exception("No es una letra");
            }
        }
    }
}
