/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import ufps.util.colecciones_seed.ListaCD;

/**
 *
 * @author Rafael Sarmiento
 */
public class Test_ConcatenarCD {
    
    public static void main(String[] args) {
    
        ListaCD<Integer> L1 = new ListaCD();
        ListaCD<Integer> L2 = new ListaCD();
    
        L1.insertarInicio(4);
        L1.insertarInicio(3);
        L1.insertarInicio(2);
        L1.insertarInicio(1);
        
        L2.insertarInicio(7);
        L2.insertarInicio(0);
        L2.insertarInicio(9);
        
        System.out.println("MI lista es:\n" + L1.toString());
        System.out.println("MI lista es:\n" + L2.toString());
        
        L1.concatenar(L2, 2);
        
        System.out.println("MI lista es:\n" + L1.toString());
        System.out.println("MI lista es:\n" + L2.toString());
    }
    
}
