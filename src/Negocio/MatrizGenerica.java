
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

/**
 *
 * @author madarme
 */
public class MatrizGenerica<T> {

    private Secuencia[] filas;

    public MatrizGenerica() {
    }

    /**
     * Constructor con procesamiento de cadenas del tipo: elemento, .... ;
     * elemento, .... ; ....
     *
     * @param cadena un String con la información de la matriz
     */
    public MatrizGenerica(int cantFilas) {

        this.filas = new Secuencia[cantFilas];
    }

    public Secuencia[] getFilas() {
        return filas;
    }

    public void setFilas(Secuencia[] filas) {
        this.filas = filas;
    }

    private void validar(int i) {
        if (i < 0 || i >= this.filas.length) {
            throw new RuntimeException("Índice de la fila fuera de rango:" + i);
        }
    }

    public Secuencia getFila(int i) {

        this.validar(i);
        return this.filas[i];
    }

    public void adicionarColumna(int i, Secuencia listaNueva) {
        this.validar(i);
        this.filas[i] = listaNueva;
    }

    @Override
    public String toString() {
        String msg = "";
        for (Secuencia lista : this.filas) {
            msg += lista.toString() + "\n";

        }
        return msg;
    }

    /**
     * Retorna la cantidad de filas
     *
     * @return un entro con la cantidad de filas
     */
    public int length() {
        return this.filas.length;
    }

    /**
     * Retorna el tipo de matriz: Cuadrada, rectangular o dispersa
     *
     * @return una cadena con el tipo de matriz
     */
    public String getTipo() {
        

        if (this == null) {
            throw new RuntimeException("La matriz esta vacia");
        }

        int columnas = 0;
        int caso = 0;

        for (Secuencia lista : this.filas) {

            if (this.length() == lista.length()) {
                if (caso == 0 || caso == 1) {
                    caso = 1;
                }
            } else {
                if (caso == 1) {
                    caso = 2;
                } else {
                    if (caso == 0) {
                        columnas = lista.length();
                    } else {
                        if (columnas == lista.length()) {
                            caso = 2;
                            columnas = lista.length();
                        } else {
                            caso = 3;
                        }
                    }
                }
            }
        }

        switch (caso) {
            case 1:
                return "Es cuadrada";

            case 2:
                return "Es rectangular";

            case 3:
                return "Es dispersa";
        }
        return null;
    }
}

