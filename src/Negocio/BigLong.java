/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import ufps.util.colecciones_seed.ListaS;

/**

 * Representa un número grande
 *
 * @author madar
 */
public class BigLong {

    ListaS<Short> numero = new ListaS();

    public BigLong() {
    }

    /**
     * Convierte la cadena a una lista de numero Ejemplo: num=259896 , la lista
     * es: <2,5,9,8,9,6>
     *
     * @param num
     */
    public BigLong(String num) {
        char[] numeros = num.toCharArray();
        for(int i = 0; i< numeros.length ;i++){
            this.numero.insertarInicio((short)numeros[i]);
        }
        
        
    }
    

    /**
     * Utilizando iteradores para la suma , NO SE PUEDE UTILIZAR VECTOR
     * 
     * realiza la suma de dos elementos y los devuelve en una lista BigLong
     * se asume que el primer elemento será el mayor
     * @param numero2 
     * @return un BigLong con la suma
     */
    public BigLong getSuma(BigLong numero2) {
        BigLong suma = new BigLong();
        this.numero.invertir();
        //numero2.numero.invertir();
        short s=0;
        for (short dato : numero) {
            for (short dato2 : numero2.numero){
                //se supone que seria (9,9)+(1)= (9+1,9)=(0,9+1)=(0,0,1) y se invierte = 100
                s = (short) (dato + dato2);//creí que la tenia y me bloquee
                if(s > 10){
                    s = (short) (s/ 10);
                }
            }
        }
        // :) ;
        return suma;
    }

    public BigLong getResta(BigLong numero2) {

        // :) ;
        return null;
    }

    @Override
    public String toString() {
        return this.numero.toString();
    }

}
