/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Objects;

/**
 *  Esto representa una persona con su fecha de nacimiento
 * 
 * @author Rafael S
 */


public class Persona implements Comparable{
    
    private int cedula;
    //Fecha de nacimiento
    private short agno,mes,dia,h,m,s;
    private String nombresCompletos;
    
    /**
     * Constructor vacío para la clase persona     * 
     */

    public Persona() {        
    }
    
    /**
     * Constructor de una persona con sus datos basicos
     * 
     * @param cedula es un entero con el numero de cedula de la persona
     * @param nombresCompletos es un String con el nombre completo de la persona
     * @param agno es un short con el año de la fecha de nacimiento de la persona
     * @param mes es un short con el mes de la fecha de nacimiento de la persona
     * @param dia es un short con el dia de la fecha de nacimiento de la persona
     * @param h es un short con la hora de la fecha de nacimiento de la persona
     * @param m es un short con el minuto de la fecha de nacimiento de la persona
     * @param s es un short con el segundo de la fecha de nacimiento de la persona
     */
    
    
    public Persona(int cedula, short agno, short mes, short dia, short h, short m, short s, String nombresCompletos) {
        this.cedula = cedula;
        this.agno = agno;
        this.mes = mes;
        this.dia = dia;
        this.h = h;
        this.m = m;
        this.s = s;
        this.nombresCompletos = nombresCompletos;
    }
    
    /**
     * Obtiene la cedula de la persona
     * 
     * @return un entero con el numero de cedula 
     */

    public int getCedula() {
        return cedula;
    }
    
    /**
     * Actualiza el valor de la cedula
     * 
     * @param cedula un entero con el valor de la cedula
     */

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    /**
     * Obtiene el año de la fecha de nacimiento
     * 
     * @return un short con el año de nacimiento 
     */
    
    public short getAgno() {
        return agno;
    }
    
    /**
     * Actualiza el valor del año de nacimiento
     * 
     * @param agno un short con el año de nacimiento
     */

    public void setAgno(short agno) {
        this.agno = agno;
    }
    
    /**
     * Obtiene el mes de la fecha de nacimiento
     * 
     * @return un short que contiene el mes de nacimiento
     */

    public short getMes() {
        return mes;
    }

    /**
     * Actualiza el valor del mes de nacimiento
     * 
     * @param mes un short con el mes de nacimiento
     */
    
    public void setMes(short mes) {
        this.mes = mes;
    }
    
    /**
     * Obtiene el valor del dia de nacimiento
     * 
     * @return un short con el dia de nacimiento
     */

    public short getDia() {
        return dia;
    }
    
    /**
     * Actualiza el valor del dia de nacimiento
     * 
     * @param dia un short con el valor del dia de nacimiento
     */

    public void setDia(short dia) {
        this.dia = dia;
    }
    /**
     * Obtiene el valor de la hora de nacimiento
     * @return un short con la hora de nacimiento
     */

    public short getH() {
        return h;
    }
    
    /**
     * Actualiza el valor de la hora de nacimiento
     * @param h un short con el valor de la hora de nacimiento
     */

    public void setH(short h) {
        this.h = h;
    }
    
    /**
     * Obtiene el valor del minuto de nacimiento
     * @return un short con el minuto de nacimiento
     */

    public short getM() {
        return m;
    }
    
    /**
     * Actuliza el valor del minuto de nacimiento
     * @param m un short con el minuto de nacimiento
     */

    public void setM(short m) {
        this.m = m;
    }
    
    /**
     * Obtiene el valor del segundo de nacimiento
     * @return un short con el segundo de nacimiento
     */

    public short getS() {
        return s;
    }
    
    /**
     * Actualiza el valor del segundo de nacimiento
     * @param s un short con el segundo de nacimiento
     */

    public void setS(short s) {
        this.s = s;
    }
    
    /**
     * Obtiene el nombre completo de la persona
     * @return un String con el nombre de la persona
     */

    public String getNombresCompletos() {
        return nombresCompletos;
    }
    
    /**
     * Actualiza el nombre de la persona
     * @param nombresCompletos un String con el nombre completo de la persona
     */

    public void setNombresCompletos(String nombresCompletos) {
        this.nombresCompletos = nombresCompletos;
    }
    
    /**
     * convierte dos variables short a float y luego realiza una divison de convercion
     * @param a un short
     * @param b un short
     * @param divisor un entero que dividira a los short
     * @return un float con los valores convertidos y sumados
     */
    public float convercion(short a, short b, int divisor){
        float c = a;
        float d = b;
        c = c / divisor;
        d = d + c;        
        return d;
    }
    
    /**
     * Es un metedo para transformar la fecha de nacimiento a una edad
     * @param agno un short con el año de nacimiento
     * @param mes un short con el mes de nacimiento
     * @param dia un short con el dia de nacimiento
     * @param hora un short con la hora de nacimiento
     * @param minuto un short con el minuto de nacimiento
     * @param segundo un short con el segundo de nacimiento
     * @return un float con la edad de la persona 
     */
    public float calcularEdad(short agno, short mes, short dia, short hora, short minuto, short segundo){
        float j = convercion(segundo,minuto,60);
        j = (j / 60) + hora;
        j = j / 24;
        j= j + dia;
        j = j / 30; 
        j =  j / 12;
        float i = convercion(mes, agno,12);
        i = i + j;
        i = 2021 - i;
        
        return i;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.cedula;
        hash = 89 * hash + this.agno;
        hash = 89 * hash + this.mes;
        hash = 89 * hash + this.dia;
        hash = 89 * hash + this.h;
        hash = 89 * hash + this.m;
        hash = 89 * hash + this.s;
        hash = 89 * hash + Objects.hashCode(this.nombresCompletos);
        return hash;
    }

    /**
     * nos permite saber si un objeto es igual a otro
     * @param obj
     * @return un booleano
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (this.cedula != other.cedula) {
            return false;
        }
        if (this.agno != other.agno) {
            return false;
        }
        if (this.mes != other.mes) {
            return false;
        }
        if (this.dia != other.dia) {
            return false;
        }
        if (this.h != other.h) {
            return false;
        }
        if (this.m != other.m) {
            return false;
        }
        if (this.s != other.s) {
            return false;
        }
        if (!Objects.equals(this.nombresCompletos, other.nombresCompletos)) {
            return false;
        }
        return true;
    }
    /**
     * es un to String de los datos de la persona
     * @return un String con todos los datos acomodados de la persona en cuestion.
     */

    @Override
    public String toString() {
        return "La persona: " + nombresCompletos + "\ncon cedula: " + cedula + "\nnacido el: " + dia +"/"+ mes + "/" + agno + " a las " + h + ":" + m + " con " + s + " segundos. "  ;
    }
    
    
    
    
   
    /**
     * El compare to se usa para saber si una persona es mayor a otra
     * @param o un objeto o que recibe otro objeto
     * @return un entero siendo 0, 1 o -1 dependiendo de si es mayor o menor
     */

    @Override
    public int compareTo(Object o) {
        
        if(this == o){
            return 0;
        }
        
        if(o == null){
            throw new RuntimeException("Ud no puso a una persona");
        }
        if (getClass() != o.getClass()) {
            throw new RuntimeException("UD está realizando comparaciones con objetos de clases distintas");
        }
        final Persona other = (Persona) o; //Se usa el middle casting para poder usar las variables internas dentro del objeto o del compare
        if (calcularEdad(this.agno, this.mes, this.dia, this.h, this.m, this.s ) > calcularEdad(other.agno, other.mes, other.dia, other.h, other.m, other.s ) ){
            return 1;
        }
        if (calcularEdad(this.agno, this.mes, this.dia, this.h, this.m, this.s ) < calcularEdad(other.agno, other.mes, other.dia, other.h, other.m, other.s ) ){
            return -1;
        }
        return 0;
        
    }
    
    
    
}
